jest.mock('../../../../lib/validation/periodical', () => ({
  periodicalNameValidators: [],
}));
jest.mock('../../../../lib/validation/validators', () => ({
  isValid: jest.fn().mockReturnValue(true),
}));
jest.mock('../../src/services/account', () => ({
  getSession: jest.fn().mockReturnValue(Promise.resolve({ identifier: 'arbitrary account id' })),
  getToken: jest.fn().mockReturnValue(Promise.resolve('Arbitrary token')),
}));
jest.mock('../../src/services/fetchResource', () => ({
  fetchResource: jest.fn().mockReturnValue({ json: jest.fn().mockReturnValue(Promise.resolve({})) }),
}));

import {
  getArticleUploadUrl,
  getDashboard,
  getHeaderUploadUrl,
  getPeriodical,
  getPeriodicalContents,
  getPeriodicals,
  getRecentScholarlyArticles,
  getScholarlyArticle,
  getScholarlyArticlesForAccount,
  initialiseArticle,
  initialiseJournal,
  isPeriodicalSlugAvailable,
  makePeriodicalPublic,
  publishScholarlyArticle,
  submitScholarlyArticle,
  updatePeriodical,
  updateScholarlyArticle,
} from '../../src/services/periodical';

describe('initialiseJournal', () => {
  it('should reject when the user does not have an account', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(initialiseJournal()).rejects.toBeDefined();
  });

  it('should reject when the journal could not be initialised', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    return expect(initialiseJournal()).rejects.toBeDefined();
  });

  it('should pass a JWT when initialising a journal', (done) => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Some token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary initialised journal',
    }));

    initialiseJournal();

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][1].headers).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBe('Bearer Some token');
      done();
    });
  });

  it('should return the initialised journal', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary initialised journal',
    }));

    return expect(initialiseJournal()).resolves.toBe('Arbitrary initialised journal');
  });
});

describe('getPeriodical', () => {
  it('should return the fetched Periodical', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary journal',
    }));

    return expect(getPeriodical('arbitrary ID')).resolves.toBe('Arbitrary journal');
  });

  it('should pass a JWT to include non-public results', (done) => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Some token'));

    getPeriodical('arbitrary ID');

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][1].headers).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBe('Bearer Some token');
      done();
    });
  });

  it('should call the back-end without JWT if none could be fetched', (done) => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getToken.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    getPeriodical('arbitrary ID');

    setImmediate(() => {
      expect(mockedFetchResource.fetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.fetchResource.mock.calls[0][0]).not.toMatch('jwt');
      done();
    });
  });

  it('should return reject in case of errors', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    return expect(getPeriodical('arbitrary ID')).rejects.toBe('Arbitrary error');
  });
});

describe('getPeriodicals', () => {
  it('should return the fetched Periodicals', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary journals',
    }));

    return expect(getPeriodicals()).resolves.toBe('Arbitrary journals');
  });

  it('should return reject in case of errors', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    return expect(getPeriodicals()).rejects.toBe('Arbitrary error');
  });
});

describe('isPeriodicalSlugAvailable', () => {
  it('should return true if no periodical could be found with the given slug', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => ({ error: { description: 'None found' } }),
    }));

    return expect(isPeriodicalSlugAvailable('arbitrary slug')).resolves.toBe(true);
  });

  it('should return false if a periodical could be found with the given slug', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => ({ result: { identifier: 'arbitrary_slug' } }),
    }));

    return expect(isPeriodicalSlugAvailable('arbitrary_slug')).resolves.toBe(false);
  });

  it('should reject in case of errors', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    return expect(getPeriodical('arbitrary ID')).rejects.toBe('Arbitrary error');
  });
});

describe('getPeriodicalContents', () => {
  it('should return the fetched articles', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary response',
    }));

    return expect(getPeriodicalContents('arbitrary ID')).resolves.toBe('Arbitrary response');
  });

  it('should return reject in case of errors', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    return expect(getPeriodicalContents('arbitrary ID')).rejects.toBe('Arbitrary error');
  });
});

describe('getHeaderUploadUrl', () => {
  it('should reject when the user does not have an account', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(getHeaderUploadUrl('arbitrary ID', 'arbitrary filename')).rejects
      .toBeDefined();
  });

  it('should reject when the user does not have a valid JSON Web Token', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getToken.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(getHeaderUploadUrl('arbitrary ID', 'arbitrary filename')).rejects
      .toBeDefined();
  });

  it('should pass the new values and a JWT to the back-end', (done) => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Some token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary response',
    }));

    getHeaderUploadUrl('some_periodical_id', 'some_filename');

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][0]).toBe('/periodicals/some_periodical_id/upload/header');
      expect(mockedFetchResource.mock.calls[0][1].headers).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBe('Bearer Some token');
      expect(mockedFetchResource.mock.calls[0][1].body).toBeDefined();
      expect(JSON.parse(mockedFetchResource.mock.calls[0][1].body)).toEqual({
        result: {
          image: 'some_filename',
        },
        targetCollection: { identifier: 'some_periodical_id' },
      });

      done();
    });
  });
});

describe('getDashboard', () => {
  it('should return the fetched dashboard content', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary response',
    }));

    return expect(getDashboard()).resolves.toBe('Arbitrary response');
  });

  it('should pass a JWT', (done) => {

    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Some token'));


    getDashboard();

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][1].headers).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBe('Bearer Some token');
      done();
    });
  });

  it('should return reject in case of errors', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    return expect(getDashboard()).rejects.toBe('Arbitrary error');
  });
});

describe('getScholarlyArticle', () => {
  it('should return the fetched article', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary article',
    }));

    return expect(getScholarlyArticle('arbitrary ID')).resolves.toBe('Arbitrary article');
  });

  it('should pass a JWT to include non-public results', (done) => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Some token'));

    getScholarlyArticle('arbitrary ID');

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][1].headers).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBe('Bearer Some token');
      done();
    });
  });

  it('should call the back-end without JWT if none could be fetched', (done) => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getToken.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    getScholarlyArticle('arbitrary ID');

    setImmediate(() => {
      expect(mockedFetchResource.fetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.fetchResource.mock.calls[0][0]).not.toMatch('jwt');
      done();
    });
  });

  it('should return reject in case of errors', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    return expect(getScholarlyArticle('arbitrary ID')).rejects.toBe('Arbitrary error');
  });
});

describe('getRecentScholarlyArticles', () => {
  it('should return the fetched articles', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => [
        { identifier: 'Arbitrary article 1' },
        { identifier: 'Arbitrary article 2' },
      ],
    }));

    return expect(getRecentScholarlyArticles()).resolves
      .toEqual([
        { identifier: 'Arbitrary article 1' },
        { identifier: 'Arbitrary article 2' },
      ]);
  });

  it('should return reject in case of errors', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    return expect(getRecentScholarlyArticles()).rejects.toBe('Arbitrary error');
  });
});

describe('getScholarlyArticlesForAccount', () => {
  it('should return the fetched articles', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => [
        { identifier: 'Arbitrary article 1' },
        { identifier: 'Arbitrary article 2' },
      ],
    }));

    return expect(getScholarlyArticlesForAccount('arbitrary account ID')).resolves
      .toEqual([
        { identifier: 'Arbitrary article 1' },
        { identifier: 'Arbitrary article 2' },
      ]);
  });

  it('should return reject in case of errors', () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    return expect(getScholarlyArticlesForAccount('arbitrary account ID')).rejects.toBe('Arbitrary error');
  });
});

describe('updateScholarlyArticle', () => {
  it('should reject when the user does not have an account', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(updateScholarlyArticle('arbitrary ID', {
      description: 'Arbitrary description',
      name: 'Arbitrary name',
    })).rejects
      .toBeDefined();
  });

  it('should reject when the user does not have a valid JSON Web Token', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getToken.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(updateScholarlyArticle('arbitrary ID', {
      description: 'Arbitrary description',
      name: 'Arbitrary name',
    })).rejects
      .toBeDefined();
  });

  it('should pass the new values and a JWT to the back-end', (done) => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Some token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary response',
    }));

    updateScholarlyArticle('article_id', {
      description: 'Some description',
      name: 'Some name',
    });

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][0]).toBe('/periodicals/articles/article_id');
      expect(mockedFetchResource.mock.calls[0][1].headers).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBe('Bearer Some token');
      expect(mockedFetchResource.mock.calls[0][1].body).toBeDefined();
      expect(JSON.parse(mockedFetchResource.mock.calls[0][1].body).object).toBeDefined();
      expect(JSON.parse(mockedFetchResource.mock.calls[0][1].body).object).toEqual({
        description: 'Some description',
        name: 'Some name',
      });

      done();
    });
  });
});

describe('getArticleUploadUrl', () => {
  it('should reject when the user does not have an account', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(getArticleUploadUrl('arbitrary ID', 'arbitrary filename', true)).rejects
      .toBeDefined();
  });

  it('should reject when the user does not have a valid JSON Web Token', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getToken.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(getArticleUploadUrl('arbitrary ID', 'arbitrary filename', true)).rejects
      .toBeDefined();
  });

  it('should pass the new values and a JWT to the back-end', (done) => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Some token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary response',
    }));

    getArticleUploadUrl('some_article_id', 'some_filename', true);

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][0]).toBe('/periodicals/articles/some_article_id/upload');
      expect(mockedFetchResource.mock.calls[0][1].headers).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBe('Bearer Some token');
      expect(mockedFetchResource.mock.calls[0][1].body).toBeDefined();
      expect(JSON.parse(mockedFetchResource.mock.calls[0][1].body)).toEqual({
        result: {
          associatedMedia: {
            license: 'https://creativecommons.org/licenses/by/4.0/',
            name: 'some_filename',
          },
        },
        targetCollection: { identifier: 'some_article_id' },
      });

      done();
    });
  });

  it('should not send to the back-end that the user agreed to the license when they did not', (done) => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Arbitrary token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary response',
    }));

    getArticleUploadUrl('some_article_id', 'some_filename', false as any);

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(JSON.parse(mockedFetchResource.mock.calls[0][1].body).result.associatedMedia.license).toBeNull();

      done();
    });
  });
});

describe('publishScholarlyArticle', () => {
  it('should reject when the user does not have an account', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(publishScholarlyArticle('arbitrary article ID')).rejects
      .toBeDefined();
  });

  it('should reject when the user does not have a valid JSON Web Token', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getToken.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(publishScholarlyArticle('arbitrary article ID')).rejects
      .toBeDefined();
  });

  it('should pass the new values and a JWT to the back-end', (done) => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Some token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary response',
    }));

    publishScholarlyArticle('some_article_id');

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][0]).toBe('/periodicals/articles/some_article_id/publish');
      expect(mockedFetchResource.mock.calls[0][1].headers).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBe('Bearer Some token');
      expect(mockedFetchResource.mock.calls[0][1].body).toBeDefined();
      expect(JSON.parse(mockedFetchResource.mock.calls[0][1].body)).toEqual({
        targetCollection: { identifier: 'some_article_id' },
      });

      done();
    });
  });
});

describe('submitScholarlyArticle', () => {
  it('should reject when the user does not have an account', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(submitScholarlyArticle('arbitrary article ID', 'arbitrary periodical ID')).rejects
      .toBeDefined();
  });

  it('should reject when the user does not have a valid JSON Web Token', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getToken.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(submitScholarlyArticle('arbitrary article ID', 'arbitrary periodical ID')).rejects
      .toBeDefined();
  });

  it('should pass the new values and a JWT to the back-end', (done) => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Some token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary response',
    }));

    submitScholarlyArticle('some_article_id', 'some_periodical_id');

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][0]).toBe('/periodicals/some_periodical_id/submit/some_article_id');
      expect(mockedFetchResource.mock.calls[0][1].headers).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBe('Bearer Some token');
      expect(mockedFetchResource.mock.calls[0][1].body).toBeDefined();
      expect(JSON.parse(mockedFetchResource.mock.calls[0][1].body)).toEqual({
        result: {
          isPartOf: {
            identifier: 'some_periodical_id',
          },
        },
        targetCollection: { identifier: 'some_article_id' },
      });

      done();
    });
  });
});

describe('updatePeriodical', () => {
  it('should reject when the user does not have an account', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(updatePeriodical('arbitrary ID', {
      description: 'Arbitrary description',
      headline: 'Arbitrary headline',
      name: 'Arbitrary name',
    })).rejects
      .toBeDefined();
  });

  it('should reject when the user does not have a valid JSON Web Token', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getToken.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(updatePeriodical('arbitrary ID', {
      description: 'Arbitrary description',
      headline: 'Arbitrary headline',
      name: 'Arbitrary name',
    })).rejects
      .toBeDefined();
  });

  it('should pass the new values and a JWT to the back-end', (done) => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Some token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary response',
    }));

    updatePeriodical('periodical_id', {
      description: 'Arbitrary description',
      headline: 'Arbitrary headline',
      name: 'Arbitrary name',
    });

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][0]).toBe('/periodicals/periodical_id');
      expect(mockedFetchResource.mock.calls[0][1].headers).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBe('Bearer Some token');
      expect(mockedFetchResource.mock.calls[0][1].body).toBeDefined();
      expect(JSON.parse(mockedFetchResource.mock.calls[0][1].body).object).toBeDefined();
      expect(JSON.parse(mockedFetchResource.mock.calls[0][1].body).object).toEqual({
        description: 'Arbitrary description',
        headline: 'Arbitrary headline',
        name: 'Arbitrary name',
      });

      done();
    });
  });
});

describe('makePeriodicalPublic', () => {
  it('should not save invalid names', () => {
    const mockedValidator = require.requireMock('../../../../lib/validation/validators');
    mockedValidator.isValid.mockReturnValueOnce(false);

    return expect(makePeriodicalPublic('arbitrary periodical ID', 'arbitrary slug')).rejects
      .toEqual(new Error('This is not a valid Journal link'));
  });

  it('should reject when the user does not have an account', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(makePeriodicalPublic('arbitrary periodical ID', 'arbitrary slug')).rejects
      .toBeDefined();
  });

  it('should reject when the user does not have a valid JSON Web Token', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getToken.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(makePeriodicalPublic('arbitrary periodical ID', 'arbitrary slug')).rejects
      .toBeDefined();
  });

  it('should pass the Journal slug and a JWT to the back-end', (done) => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Some token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary response',
    }));

    makePeriodicalPublic('periodical_id', 'some_slug');

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][0]).toBe('/periodicals/periodical_id/publish');
      expect(mockedFetchResource.mock.calls[0][1].headers).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBe('Bearer Some token');
      expect(JSON.parse(mockedFetchResource.mock.calls[0][1].body).object).toBeDefined();
      expect(JSON.parse(mockedFetchResource.mock.calls[0][1].body).object.identifier).toBeDefined();
      expect(JSON.parse(mockedFetchResource.mock.calls[0][1].body).object.identifier).toBe('some_slug');
      done();
    });
  });
});

describe('initialiseArticle', () => {
  it('should reject when the user does not have an account', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.reject('arbitrary error'));

    return expect(initialiseArticle('Arbitrary periodical ID')).rejects.toBeDefined();
  });

  it('should reject when the article could not be initialised', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.reject('Arbitrary error'));

    return expect(initialiseArticle('Arbitrary periodical ID')).rejects.toBeDefined();
  });

  it('should pass a JWT when initialising a journal', (done) => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    mockedAccount.getToken.mockReturnValueOnce(Promise.resolve('Some token'));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource').fetchResource;
    mockedFetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Arbitrary initialised article',
    }));

    initialiseArticle('Arbitrary periodical ID');

    setImmediate(() => {
      expect(mockedFetchResource.mock.calls.length).toBe(1);
      expect(mockedFetchResource.mock.calls[0][1].headers).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBeDefined();
      expect(mockedFetchResource.mock.calls[0][1].headers.get('Authorization')).toBe('Bearer Some token');
      done();
    });
  });

  it('should initialise an article without a periodical ID when none was provided', async () => {
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');

    await initialiseArticle();

    const requestBody = JSON.parse(mockedFetchResource.fetchResource.mock.calls[0][1].body);

    return expect(requestBody.result.isPartOf).toBeUndefined();
  });

  it('should return the initialised article', () => {
    const mockedAccount = require.requireMock('../../src/services/account');
    mockedAccount.getSession.mockReturnValueOnce(Promise.resolve({ identifier: 'arbitrary account id' }));
    const mockedFetchResource = require.requireMock('../../src/services/fetchResource');
    mockedFetchResource.fetchResource.mockReturnValueOnce(Promise.resolve({
      json: () => 'Some initialised article',
    }));

    return expect(initialiseArticle('Arbitrary periodical ID')).resolves.toBe('Some initialised article');
  });
});
